# keyboard arrangement problem using genetic algorithm
svmihar, 2019    

[DATASET](https://github.com/dwyl/english-words/blob/master/words_alpha.txt)

TODO
- [ ] buat keyboard layout
- [ ] buat character frequency
- [ ] rise rise rise rise 

## prerequisites

## eda

## genetic algorithm 
- [ ] penjelasan genetic algorithm yang dari classical computer problem 

## scoring system 
- [x] buat scoring tentang 2 karakter yang bersebelahan
'sebelahan' key cost
![](https://raw.githubusercontent.com/mw8/white_keyboard_layout/master/diagrams/diagram-double_metric.png)

QWERTY letters are supposedly arranged that way to prevent jam when typing using old typing machine. Jam happens when 2 letters close to each other are typed at almost the same time. The QWERTY letter placement is arranged such that letters that are often occurred next to each other, to be placed separately.


## references
[using annealing and tabu search](https://github.com/mw8/white_keyboard_layout)
[genetic algorithm approach](https://github.com/jtsagata/keyboard-monkey)