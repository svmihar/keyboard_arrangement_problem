from copy import deecopy


def string_to_layout(layout_string, base_layout=None): 
    """ Turn a string into a layout
    qwert yuop
    asdfg hkjl
    zxcvb nm,./
    """
    layer_0_keys = []
    to_replace = []
    def set_key(current_key, new_letter, pos)